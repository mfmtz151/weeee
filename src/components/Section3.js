import styles from '../../styles/carolina-reyes/Section3.module.scss';
import TimeLine from './TimeLine';

export default function Section1() {
  return (
    <div className={styles.container}>
      <TimeLine></TimeLine>
    </div>
  )
}

import { Button, Card, FormControlLabel, Radio, RadioGroup } from '@mui/material';
import styles from '../../styles/carolina-reyes/Section6.module.scss';
import TextField from '@mui/material/TextField';

export default function Section5() {
    return (
        <div className={styles.container}>
            <div className={styles.title} >
                NO NIÑOS
            </div>

            <div className={styles.container2}>
                Amamos a sus pequeños, pero queremos que este día sólo tengan que 
                preocuparse por pasarla increíble y lo dejen todo en la pista.
            </div>


        </div>
    )
}

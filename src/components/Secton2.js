import styles from '../../styles/carolina-reyes/Section2.module.scss';

export default function Section2() {
    return (
        <div className={styles.container}>
            <div className={styles.title} >
                EN COMPAÑÍA DE NUESTROS PADRES
            </div>

            <div className={styles.container2} >
                <div className={styles.boySection}>
                    <div>
                        PAPÁS DEL NOVIO
                    </div>
                    <div>
                        REYES MAGDALENO VERDIALES FELIX <br></br>
                        LEOVYGILDA ONTIVEROS ASTORGA <br></br>
                    </div>
                </div>

                <div className={styles.girlSection}>
                    <div>
                        PAPÁS DEL NOVIO
                    </div>
                    <div>
                        FRANCISCO CARDENAS GRAVE <br></br>
                        MARÍA JESÚS TERRAZAS ROMERO <br></br>
                    </div>
                </div>
            </div>


        </div>
    )
}

import { Button, Card, FormControlLabel, Radio, RadioGroup } from '@mui/material';
import styles from '../../styles/carolina-reyes/Section4.module.scss';
import TextField from '@mui/material/TextField';

export default function Section2() {
    return (
        <div className={styles.container}>
            <div className={styles.title} >
                CONFIRMA TU ASISTENCIA
            </div>

            <div className={styles.container2}>


                <div className={styles.asistiras} >
                    <div className={styles.asistirasTitle} >¿Asistirás?</div>
                    <div className={styles.radioGroup}>
                        <RadioGroup
                            aria-labelledby="demo-radio-buttons-group-label"
                            defaultValue="female"
                            name="radio-buttons-group"
                        >
                            <FormControlLabel value="si" control={<Radio />} label="Si asistiré" />
                            <FormControlLabel value="no" control={<Radio />} label="No asistiré" />
                        </RadioGroup>
                    </div>
                    <div className={styles.textfield}><TextField style={{ width: '90%', marginTop: '20px' }} placeholder='Nombre de invitado' size='small' id="standard-basic" variant="outlined" /></div>
                    <div className={styles.textfield}><Button style={{ width: '90%', marginTop: '20px' }} variant="contained">Confirmar</Button></div>
                </div>



            </div>


        </div>
    )
}
